// PREPARATON DE MES CLASSES

function Produit(n, p){

    this.nom = n;
    this.prix = p;
  }

  function Panier(totalHT, totalTTC){

    this.produits = [];
    this.totalHT = 0;
    this.totalTTC = 0;
    this.TVA = 1.055;

    this.ajouter = function(p){
        
        this.produits.push(p);
        this.totalHT = this.totalHT + p.prix;
        this.totalTTC = this.totalHT * this.TVA;
    }
  }

// C'EST CA LE PROGRAMME
  
  var baguette = new Produit( 'Baguette', 0.85); // prix HT
  var croissant = new Produit( 'Croissant', 0.80);
  var panier = new Panier();

  panier.ajouter(baguette);
  panier.ajouter(croissant);
  
  
  console.log(panier);
  console.log(panier.totalHT);
  console.log(panier.totalTTC);  
